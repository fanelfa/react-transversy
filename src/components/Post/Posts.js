import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPosts } from '../../actions/postAction';


class Posts extends Component {
    componentDidMount(){
        // fecth dari api lalu menyimpannya ke store
        this.props.fetchPosts();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.newPost){
            // unshift untuk menambahkan data newpost ke awal array posts
            this.props.posts.unshift(nextProps.newPost);
        }
    }

    render() {
        const postItems = this.props.posts.map(post => (
            <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
            </div>
        ));
        return (
            <div className="Posts">
                <h1>Post</h1>
                { postItems }
            </div>
        )
    }
}

Posts.propTypes = {
    fetchPosts  : PropTypes.func.isRequired,
    posts       : PropTypes.array.isRequired,
    newPost     : PropTypes.object
};

// mengambil state dari store
// lalu mengubah state menjadi properti
const mapStateToProps = state => ({
    posts   : state.posts.items,
    newPost : state.posts.item
});

// connect untuk menghubungkan Posts ke store
export default connect(mapStateToProps, {fetchPosts})(Posts);