import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '../../actions/postAction';


class Form extends Component {
    constructor(props){
        super(props);
        this.state = {
            title : '',
            body  : '',
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e){
        // nama state harus sama dengan name di jsx
        this.setState({ [e.target.name] : e.target.value});
    }

    onSubmit(e){
        e.preventDefault();

        const post = {
            title   : this.state.title,
            body    : this.state.body,
        }
        
        // mengeksekusi fungsi createPost dari postAction
        this.props.createPost(post);
    }


    render() {
        return (
            <div>
                <h1>Post Form</h1>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label>Title :</label><br/>
                        <input type="text" name="title" onChange={this.onChange} value={this.state.title}/>
                    </div>
                    <br/>
                    <div>
                        <label>Body :</label><br/>
                        <textarea name="body" onChange={this.onChange}  value={this.state.body}/>
                    </div>
                    <br/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

// untuk memastikan tipe data dari properti
Form.propTypes = {
    createPost: PropTypes.func.isRequired
}


export default connect(null, {createPost})(Form);